<x-layout>
<h1 class="w-3/4 mx-auto text-center text-2xl font-bold">Create Info</h1>
<form method="POST" action="{{route('infos.store')}}" class="w-3/4 mx-auto py-4">
    @csrf

    <x-input input="text" name="title"></x-input>
    <x-input input="text" name="link"></x-input>
    <x-input input="text" name="tags"></x-input>
    <x-input input="textarea" name="teaser"></x-input>
    <x-input input="textarea" name="content"></x-input>

    <div class="flex justify-center gap-2">
        <input type="submit" value="Create"
            class="border-2 border-black px-2 py-1 rounded-lg hover:bg-black hover:text-white cursor-pointer">
        <a href="{{route('infos.index')}}"
            class="border-2 border-black px-2 py-1 rounded-lg hover:bg-black hover:text-white cursor-pointer">
            Back
        </a>
    </div>
</form>
</x-layout>
