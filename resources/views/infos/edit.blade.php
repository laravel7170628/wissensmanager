<x-layout>
    <h1 class="w-3/4 mx-auto text-center text-2xl font-bold">Edit Info</h1>
    <form method="POST" action="{{route('infos.update', [$info])}}" class="w-3/4 mx-auto py-4">
        @csrf
        @method('PUT')

        <x-input input="text" name="title" :model=$info></x-input>
        <x-input input="text" name="link" :model=$info></x-input>
        <x-input input="text" name="tags" :model=$info></x-input>
        <x-input input="textarea" name="teaser" :model=$info></x-input>
        <x-input input="textarea" name="content" :model=$info></x-input>

        <div class="flex justify-center gap-2">
            <input type="submit" value="Save"
                class="border-2 border-black px-2 py-1 rounded-lg hover:bg-black hover:text-white cursor-pointer">
            <a href="{{route('infos.index')}}"
                class="border-2 border-black px-2 py-1 rounded-lg hover:bg-black hover:text-white cursor-pointer">
                Back
            </a>
        </div>
    </form>
    </x-layout>
