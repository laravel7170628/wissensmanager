<x-layout>
<div class="flex justify-center mb-4">
    {{ $infos->links() }}
</div>
@forelse ($infos as $info)
<div x-data="{ show_content: false }" class="mb-12 text-black">
    <div class="black border-black border-4 rounded-md relative flex items-center">
        <div class="text-center font-medium text-xl pt-2 pb-4 px-4 w-full">
            @if ($info->link)
                <a class="underline" href="{{$info->link}}">{{ $info->title}}</a>
            @else
                {{ $info->title}}
            @endif
        </div>
        <div class="absolute z-1 -bottom-3 flex justify-center w-full text-sm gap-2">
            @foreach ($info->arrayWithTags($info) as $tag)
            <span class="bg-white border-black border-2 black rounded-lg px-2">{{$tag}}</span>
            @endforeach
        </div>
        <div class="absolute justify-center w-full -bottom-3 flex gap-1">

        </div>
        <div class="absolute -top-4 right-2 flex gap-1">
            <button x-on:click="show_content = ! show_content"
                class="border-4 border-black rounded-full bg-white w-8 text-sm font-medium
                hover:bg-black hover:text-white cursor-pointer">
                   T
            </button>
            <a href="{{route('infos.edit', ['info' => $info])}}"
                class="border-4 border-black rounded-full bg-white w-8 text-sm font-medium text-center
                hover:bg-black hover:text-white cursor-pointer">E</a>
            <form method="POST" action="{{route('infos.destroy', ['info' => $info])}}">
                @csrf
                @method('DELETE')
                <input type="submit" value="D"
                    class=" border-4 border-black rounded-full bg-white w-8 text-sm font-medium
                        hover:bg-black hover:text-white cursor-pointer">
            </form>
        </div>
    </div>
    <div class="relative">
        <div class="px-4">
            <div class="black border-black border-x-4 border-b-4 rounded-b-md pt-1">
                <div class="flex ">
                    <div class=" px-5 py-2 w-full">
                        <div class="div mb-2">
                            {{ $info->teaser}}
                        </div>
                    </div>
                </div>
                <div x-show="show_content" class="px-5 pb-4 py-2 border-black border-t-2">
                    {{ $info->content}}
                </div>
            </div>
        </div>
    </div>
</div>
@empty
<div class="text-center text-2xl">
    Keine Infos vorhanden.
</div>
@endforelse
<div class="flex justify-center mb-4">
    {{ $infos->links() }}
</div>
</x-layout>
