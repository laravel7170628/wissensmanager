@switch($input)
    @case("text")
        <div class="relative mb-6">
            <label for="{{$name}}"
                class="absolute px-2 py-0 z-1 bg-white text-sm border-2 border-black -top-4 -left-3 rounded-lg font-medium
                @error($name) text-red-600 border-red-600 @enderror">{{ucfirst($name)}}</label>
            <input id="{{$name}}" type="text" name="{{$name}}" value="{{ $model->$name ?? old($name)}}"
                class="border-2 border-black w-full px-2 outline-none rounded-lg p-1 pt-2
                @error($name) border-red-600 @enderror">
            @error($name)
            <div class="text-center text-red-600 text-sm">{{$message}}</div>
            @enderror
        </div>
        @break
    @case("textarea")
        <div class="relative mb-6">
            <label for="{{$name}}"
                class="absolute px-2 py-0 z-1 bg-white text-sm border-2 border-black -top-4 -left-3 rounded-lg font-medium
                @error($name) text-red-600 border-red-600 @enderror">{{ucfirst($name)}}</label>
            <textarea id="{{$name}}" type="text" name="{{$name}}" class="border-2 border-black w-full px-2 outline-none rounded-lg p-1 pt-2
                @error($name) border-red-600 @enderror">{{$model->$name ?? old($name)}}</textarea>
            @error($name)
            <div class="text-center text-red-600 text-sm">{{$message}}</div>
            @enderror
        </div>
        @break
@endswitch

