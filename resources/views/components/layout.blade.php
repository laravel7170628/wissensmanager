<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite('resources/css/app.css')
    <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
</head>
<body class="relative">
    <div class="container mx-auto py-4 px-2">
        {{ $slot }}
    </div>
    <nav class="fixed left-4 bottom-4 flex flex-col gap-2">
        <a href="{{route('infos.create')}}"
            class="bg-white border-4 border-black rounded-full px-3 py-1 font-bold
                hover:bg-black hover:text-white cursor-pointer">+</a>
    </nav>
</body>
</html>
