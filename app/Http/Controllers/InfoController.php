<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInfoRequest;
use App\Http\Requests\UpdateInfoRequest;
use App\Models\Info;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $infos = Info::latest()->simplePaginate(15);

        return view('infos.index', ['infos' => $infos]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('infos.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreInfoRequest $request)
    {
        Info::create($request->validated());

        return redirect()->route('infos.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Info $info)
    {
        return view('infos.edit', ['info' => $info]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateInfoRequest $request, Info $info)
    {
        $info->update($request->validated());
        return redirect()->route('infos.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Info $info)
    {
        $info->delete();
        return redirect()->route('infos.index');
    }
}
