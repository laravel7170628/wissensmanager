<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'link', 'tags', 'teaser', 'content'];

    public function arrayWithTags(Info $info){
        return explode(",", $info->tags);
    }
}
