<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Info>
 */
class InfoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->sentence(5),
            'link' => 'https://www.websimulator.de',
            'tags' => fake()->randomElement(['Wissen', 'Persönlichkeit', 'Produktivität']),
            'content' => fake()->sentence(200),
            'teaser' => fake()->sentence(40)
        ];
    }
}
